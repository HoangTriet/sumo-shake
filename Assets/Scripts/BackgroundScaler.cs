﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScaler : MonoBehaviour {

    
    

	// Use this for initialization
	void Start () {
        var worldHeight = Camera.main.orthographicSize * 2f;
        var worldWidth = worldHeight * Screen.width / Screen.height;
        transform.localScale = new Vector3(worldWidth, worldHeight, 0);
        Screen.orientation = ScreenOrientation.Portrait;

    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
